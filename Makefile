info:	      # This list
	@clear
	@echo ""
	@echo ' █████╗ ██╗ ██████╗ ███████╗'
	@echo '██╔══██╗██║██╔═══██╗██╔════╝'
	@echo '███████║██║██║   ██║███████╗'
	@echo '██╔══██║██║██║   ██║╚════██║'
	@echo '██║  ██║██║╚██████╔╝███████║'
	@echo '╚═╝  ╚═╝╚═╝ ╚═════╝ ╚══════╝'
	@echo ""
	@cat $(MAKEFILE_LIST) | grep -e "^[a-zA-Z_\-]*: *.*"

start:
	@yarn run dev