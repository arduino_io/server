import HubActions from "./HubsActions"
import { startTcpServer } from "./servers/tcp/tcp"
import { startUdpServer } from "./servers/udp"

const actions: HubActions = new HubActions()
startUdpServer(actions)
startTcpServer(actions)
