class HubActions {
	private hubsIps: Map<string, string>

	constructor() {
		this.hubsIps = new Map()
	}

	addHubIp(name: string, ip: string): void {
		this.hubsIps.set(name, ip)
	}

	getHubsIps(name = ""): string[] {
		const _name = name.trim()
		if (_name !== "") {
			const hub = this.hubsIps.get(_name)
			if (hub) {
				return [hub]
			}
		} else {
			return Array.from(this.hubsIps.values())
		}

		return []
	}

	get hubsLength(): number {
		return this.hubsIps.size
	}
}

export default HubActions
