import dgram from "dgram"
import HubActions from "../HubsActions"

const HOST = "0.0.0.0"
const PORT = 30000

function startUdpServer(actions: HubActions) {
	console.log("Starting udp...")
	const server = dgram.createSocket("udp4")

	server.on("error", (err) => {
		console.log(`server error:\n${err.stack}`)
		server.close()
	})

	server.on("message", (msg, rinfo) => {
		console.log(`Hub: ${msg} - awaken at ${rinfo.address}`)
		actions.addHubIp(String(msg), rinfo.address)
	})

	server.on("listening", () => {
		const address = server.address()
		console.log(`UDP server listening on ${address.address}:${address.port}`)
	})

	server.bind({
		address: HOST,
		port: PORT,
		exclusive: true,
	})
}

export { startUdpServer }
