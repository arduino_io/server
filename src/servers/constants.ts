const PORT_SERVER = 3000
const PORT_REMOTE = 3030
const PROTOCOL = "http"
const PARAM = "comm"

export { PORT_REMOTE, PORT_SERVER, PROTOCOL, PARAM }
