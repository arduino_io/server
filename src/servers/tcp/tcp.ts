import type { HubType } from "@arduino_io/types"
import express from "express"
import HubActions from "../../HubsActions"
import { PORT_SERVER } from "../constants"
import ArduinoConnection from "./ArduinoConnection"

const DESCRIBE = "/describe/:name?"
const READ = "/read/:hub/:component"
const WRITE = "/write/:hub/:component/:value"

function startTcpServer(actions: HubActions) {
	console.log("Starting tcp...")
	const app = express()

	const arduino = new ArduinoConnection()

	app.get(DESCRIBE, async (req, res) => {
		const ips = actions.getHubsIps(req.params.name)
		const out: HubType[] = (await arduino.getHubsByIps(ips)) ?? []
		res.send(JSON.stringify(out))
	})

	app.get(READ, async (req, res) => {
		const { hub, component } = req.params

		const ips = actions.getHubsIps(hub)
		if (ips.length === 1) {
			const out: number | boolean = await arduino.getCurrentValueFrom(ips[0], component)
			res.send({ value: out })
		} else {
			res.send("NOPE")
		}
	})

	app.get(WRITE, async (req, res) => {
		const { component, hub, value } = req.params

		const ips = actions.getHubsIps(hub)
		if (ips.length === 1) {
			let _value: boolean | number | null = null
			if (value === "true" || value === "false") {
				_value = value === "true"
			} else if (!Number.isNaN(parseInt(value))) {
				_value = parseInt(value)
			}
			if (_value !== null) {
				await arduino.setCurrentValueTo(ips[0], component, _value)
			}
			res.send("OK")
		}
	})

	app.get("/", (_, res) => {
		const size = actions.hubsLength

		res.send(`${size} arduino hub${size === 1 ? "" : "s"} ${size === 1 ? "is" : "are"} connected to this server.`)
	})

	app.listen(PORT_SERVER, () => {
		console.log(`TCP server listening on port ${PORT_SERVER}.`)
	})
}

export { startTcpServer }
