import CommandFactory from "@arduino_io/command_factory"
import type { HubType } from "@arduino_io/types"
import { Hub } from "@arduino_io/types"
import { create } from "superstruct"
import { PARAM, PORT_REMOTE, PROTOCOL } from "../constants"

function getUrl(ip: string, comm: string): string {
	return `${PROTOCOL}://${ip}:${PORT_REMOTE}/?${PARAM}=${comm}`
}

// biome-ignore lint/suspicious/noExplicitAny: <explanation>
async function fetcher<T>(url: string, converter: (d: any) => T): Promise<T | null> {
	try {
		const response = await fetch(url)
		if (response.status !== 404) {
			const data = await response.json()

			return converter(data)
		}
	} catch (error) {
		return null
	}
	return null
}

async function getHubDescription(ip: string): Promise<HubType | null> {
	const url = getUrl(ip, CommandFactory.describe())

	return await fetcher(url, (data) => {
		return create(data, Hub)
	})
}

class ArduinoConnection {
	async getHubsByIps(ips: string[]): Promise<HubType[]> {
		const out: HubType[] = []
		for (let i = 0; i < ips.length; ++i) {
			const hub = await getHubDescription(ips[i])
			if (hub) {
				out.push(hub)
			}
		}
		return out
	}

	async getCurrentValueFrom(ip: string, component: string): Promise<boolean | number> {
		const url = getUrl(ip, CommandFactory.read(component))
		const out = await fetcher(url, (data) => {
			console.log({ data })
			if (typeof data === "boolean") {
				return data
			} else {
				return parseInt(data)
			}
		})
		return out ?? 0
	}

	async setCurrentValueTo(ip: string, component: string, value: number | boolean): Promise<void> {
		const url = getUrl(ip, CommandFactory.write(component, value))
		console.log({ url })
		await fetcher(url, () => {})
	}
}

export default ArduinoConnection
