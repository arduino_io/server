# Arduino IO Server - AIOS

## Thanks

* Multicast IP - [The Geek Diary](https://www.thegeekdiary.com/how-to-configure-multicast-on-an-ip-address-interface/)
* UDP help - [Platform Engineer](https://platformengineer.com/node-js-udp-server-client-example-using-dgram/)
* Make ascii art - [TextKool](https://textkool.com/en/ascii-art-generator?hl=default&vl=default&font=ANSI%20Shadow&text=aios)
* Validate my json - [JSONLint](https://jsonlint.com/)
